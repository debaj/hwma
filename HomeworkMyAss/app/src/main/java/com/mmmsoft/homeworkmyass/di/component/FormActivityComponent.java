package com.mmmsoft.homeworkmyass.di.component;

import com.mmmsoft.homeworkmyass.activity.FormActivity;
import com.mmmsoft.homeworkmyass.di.module.FormActivityModule;
import com.mmmsoft.homeworkmyass.di.scopes.Kismacska;

import dagger.Component;

@Component(dependencies = ApiComponent.class, modules = {
    FormActivityModule.class
})
@Kismacska
public interface FormActivityComponent {

    void inject(FormActivity activity);

    final class Injector {
        private static FormActivityComponent component;

        private Injector() {

        }

        public static void inject(FormActivity activity) {
            component = DaggerFormActivityComponent.builder()
                    .build();
            component.inject(activity);
        }

        public static FormActivityComponent getComponent() {
            return component;
        }
    }
}
