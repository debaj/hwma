package com.mmmsoft.homeworkmyass.di.component;

import com.mmmsoft.homeworkmyass.activity.DetailActivity;
import com.mmmsoft.homeworkmyass.di.module.DetailActivityModule;
import com.mmmsoft.homeworkmyass.di.scopes.Kismacska;

import dagger.Component;

@Component(dependencies = ApiComponent.class, modules = {
    DetailActivityModule.class
})
@Kismacska
public interface DetailActivityComponent {

    void inject(DetailActivity activity);

    final class Injector {
        private static DetailActivityComponent component;

        private Injector() {

        }

        public static void inject(DetailActivity activity) {
            component = DaggerDetailActivityComponent.builder()
                    .build();
            component.inject(activity);
        }

        public static DetailActivityComponent getComponent() {
            return component;
        }
    }
}
