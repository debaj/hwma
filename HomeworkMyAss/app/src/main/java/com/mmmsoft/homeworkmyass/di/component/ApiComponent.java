package com.mmmsoft.homeworkmyass.di.component;

import com.mmmsoft.homeworkmyass.di.module.ApiModule;
import com.mmmsoft.homeworkmyass.di.module.NetworkingModule;
import com.mmmsoft.homeworkmyass.di.scopes.Kiskutya;

import dagger.Subcomponent;

@Subcomponent(modules = {
    ApiModule.class,
    NetworkingModule.class
})
@Kiskutya
public interface ApiComponent {

    @Subcomponent.Builder
    interface Builder {
        Builder apiModule(ApiModule module);

        Builder networkingModule(NetworkingModule module);

        ApiComponent build();
    }
}