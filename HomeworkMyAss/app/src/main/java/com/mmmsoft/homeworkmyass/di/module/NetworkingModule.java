package com.mmmsoft.homeworkmyass.di.module;

import com.mmmsoft.homeworkmyass.di.scopes.Kiskutya;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class NetworkingModule {
    @Provides
    @Kiskutya
    OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder().build();
    }
}
