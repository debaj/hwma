package com.mmmsoft.homeworkmyass.di.component;

import com.mmmsoft.homeworkmyass.activity.ListActivity;
import com.mmmsoft.homeworkmyass.di.module.ListActivityModule;
import com.mmmsoft.homeworkmyass.di.scopes.Kismacska;

import dagger.Component;

@Component(dependencies = ApiComponent.class, modules = {
    ListActivityModule.class
})
@Kismacska
public interface ListActivityComponent {

    void inject(ListActivity activity);

    final class Injector {
        private static ListActivityComponent component;

        private Injector() {

        }

        public static void inject(ListActivity activity) {
            component = DaggerListActivityComponent.builder()
                    .build();
            component.inject(activity);
        }

        public static ListActivityComponent getComponent() {
            return component;
        }
    }

}
