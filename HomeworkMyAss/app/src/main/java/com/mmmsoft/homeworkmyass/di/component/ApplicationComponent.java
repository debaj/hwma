package com.mmmsoft.homeworkmyass.di.component;

import javax.inject.Singleton;

import com.mmmsoft.homeworkmyass.HWMAApp;
import com.mmmsoft.homeworkmyass.di.module.ApplicationModule;

import android.app.Application;
import android.content.res.Resources;
import android.view.LayoutInflater;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(HWMAApp app);

    Application application();

    Resources resources();

    LayoutInflater layoutInflater();

    final class Injector {
        private static ApplicationComponent applicationComponent;

        private Injector() {

        }

        public static void inject(HWMAApp app) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(app))
                    .build();
            applicationComponent.inject(app);
        }

        public static ApplicationComponent getComponent() {
            return applicationComponent;
        }
    }

}
