package com.mmmsoft.homeworkmyass;

import android.app.Application;

import com.mmmsoft.homeworkmyass.di.component.ApplicationComponent;

public class HWMAApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponent.Injector.inject(this);
    }
}
